package com.example.turnercruz.mountainpower

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.turnercruz.mountainpower.ResortFragment
import com.example.turnercruz.mountainpower.dummy.DummyContent
import android.net.Uri
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_resort_list.*

class ResortList : AppCompatActivity(), ResortFragment.OnListFragmentInteractionListener {

    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {

        val startResortActivity = Intent(this, ResortActivity::class.java)
        //startPDIntent.putExtra("id",item)
        startActivityForResult(startResortActivity, 909)
    }

    override fun onCreate( savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resort_list)
        //setSupportActionBar(toolbar)


    }

}
