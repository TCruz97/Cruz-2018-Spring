package com.example.turnercruz.recyclerview

import android.widget.EditText
import ninja.sakib.pultusorm.annotations.AutoIncrement
import ninja.sakib.pultusorm.annotations.PrimaryKey

/**
 * Created by turnercruz on 4/13/18.
 */
class ImageDetails {
    @PrimaryKey
    @AutoIncrement


    var title: String =""

    var description: String=""
}

