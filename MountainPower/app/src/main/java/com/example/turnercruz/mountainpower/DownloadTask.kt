package com.example.turnercruz.mountainpower

import android.os.AsyncTask
import java.net.HttpURLConnection
import java.net.URL
import android.system.Os.link
import org.json.JSONObject
import java.io.InputStream
import java.io.InputStreamReader


class DownloadTask: AsyncTask<String, Void, String>(){


    override fun doInBackground(vararg urls: String?): String? {

        var result:String = ""
        lateinit var url:URL
        lateinit var connection: HttpURLConnection
        try{
            url = (URL(urls[0]))
            connection = url.openConnection() as HttpURLConnection
            val ip = connection.getInputStream()
            val ir:InputStreamReader = InputStreamReader(ip)

            var data:Int = ir.read()

            while (data != -1){
                var current: Char = data as Char
                result += current
                data = ir.read()
            }
            return  result

        }
        catch (e:Exception){
            e.printStackTrace()
        }
        return null

    }

    protected override fun  onPostExecute(result:String){


        super.onPostExecute(result)

        try{

            val jsObject:JSONObject = JSONObject(result)

            val weatherData: JSONObject = JSONObject(jsObject.getString("main"))
            //temp data
            val temperature:Double = weatherData.getString("temp") as Double
            val tempAsInt:Int = (temperature * (1.8-459.67)) as Int
            //wind data
            val wind:Double = weatherData.getString("wind") as Double
            val windAsInt:Int = (wind) as Int



            ResortActivity.tempDisplay.setText(tempAsInt)
            ResortActivity.windDisplay.setText(windAsInt)




        }
        catch (e:Exception){
            e.printStackTrace()
        }


    }


}