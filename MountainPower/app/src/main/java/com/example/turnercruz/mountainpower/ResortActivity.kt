package com.example.turnercruz.mountainpower

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.TextView
import java.text.DateFormat
import java.util.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.Console
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL



class ResortActivity : AppCompatActivity() {


    companion object {
        lateinit var tempDisplay:TextView
        lateinit var windDisplay:TextView

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resort)
        if (ActivityCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            return
        }
        val locationManager:LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val provider:String = locationManager.getBestProvider(Criteria(), false)
        val location:Location = locationManager.getLastKnownLocation(provider)
        val lat:Double= location.latitude
        val lon:Double = location.longitude

        val locationText:String = ("$lat  $lon")

//        val url:URL= ("http://samples.openweathermap.org/data/2.5/weather?lat=44&lon=116&appid=351edb7c80824563330cb0b5d692a8a5") as URL


        val task:DownloadTask = DownloadTask()
        task.execute("http://samples.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=351edb7c80824563330cb0b5d692a8a5")

        val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())

        val dateDisplay: TextView = findViewById(R.id.dateText)

        dateDisplay.text = currentDateTimeString

        tempDisplay= findViewById(R.id.temp)
        windDisplay = findViewById(R.id.wind)

        MainActivity.locationView.text = locationText










    }



}
