package com.example.turnercruz.recyclerview

import java.io.Serializable
import java.util.*

/**
 * Created by turnercruz on 4/14/18.
 */
data class ImageObject (val title:String,
                        val description:String): Serializable {
}