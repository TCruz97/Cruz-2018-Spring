package com.example.turnercruz.mountainpower

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
companion object {
     lateinit var locationView: TextView
}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val submit: Button = findViewById(R.id.okButton)
         locationView = findViewById(R.id.locationID)



        val startSecondActivity: Intent = Intent(this, ResortList::class.java)

        submit.setOnClickListener {
            startActivity(startSecondActivity)

        }
    }


}
